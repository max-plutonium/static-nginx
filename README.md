[![pipeline status](https://gitlab.com/max-plutonium/static-nginx/badges/master/pipeline.svg)](https://gitlab.com/max-plutonium/static-nginx/-/commits/master) 

Static NGINX
============

NGINX server for serve static files.
