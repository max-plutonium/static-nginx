FROM nginx:alpine3.17

RUN apk update && apk add openssl

RUN openssl dhparam -out /etc/nginx/dhparam.pem 2048 && cat /etc/nginx/dhparam.pem
RUN mkdir -p /etc/nginx/sites
COPY nginx.conf /etc/nginx

EXPOSE 80 443

STOPSIGNAL SIGTERM

CMD ["nginx"]
